systemctl --failed
systemctl status
journalctl -p 3 -xb

# List orphan packages
pacman -Qtdq

# List config files diverged from package
pacman -Qii | awk '/^MODIFIED/ {print $2}'

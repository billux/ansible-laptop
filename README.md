This Ansible playbook is designed to set up the system after a fresh Arch Linux
install on a Lenovo Thinkpad x220. The [Arch Linux
wiki](https://wiki.archlinux.org/) was my main source of documentation at the
time I wrote this playbook.

Even if one can change variables at the top of the playbook, its aim isn't to
be a generic post-install playbook but to set the system as I want it to be,
*for my own use*. Also the playbook doesn't take care of my dotfiles, which are
handled differently.

The playbook contains all things Ansible needs (variables, tasks, handlers and
files/templates) in one file so that it can be easily copied to a new machine.
